package me.newkad.doublej;

import me.newkad.doublej.ActionBar;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.BlockFace;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

public class DoubleJump extends JavaPlugin implements Listener {
    public List<String> PlayersInAir = new ArrayList<String>();
    public double height = 1.5;
    public double multiply = 1.5;
    public List<String> canDJ = new ArrayList<String>();
    List<Player> cooldown = new ArrayList<Player>();
    
    
    
    
    
    public void onEnable() {
        this.getServer().getPluginManager().registerEvents((Listener)this, (Plugin)this);
        this.getConfig().options().copyDefaults(true);
        ArrayList<String> list = new ArrayList<String>();
        list.add("world");
        this.getConfig().addDefault("settings.sound", (Object)"ENTITY_SLIME_JUMP");
        this.getConfig().addDefault("settings.waitsound", "ENTITY_VILLAGER_NO");
        this.getConfig().addDefault("settings.particle", (Object)"MOBSPAWNER_FLAMES");
        this.getConfig().addDefault("settings.cooldown", 100);
        this.getConfig().addDefault("settings.worlds", list);
        this.getConfig().addDefault("messages.prefix", (Object)"&6DJ §7»");
        this.getConfig().addDefault("messages.specify-argument", (Object)"&cPlease specify an argument ! (help, reload or toggle)");
        this.getConfig().addDefault("messages.toggle-on", (Object)"&eDouble Jump toggled ON!");
        this.getConfig().addDefault("messages.toggle-off", (Object)"&eDouble Jump toggled OFF!");
        this.getConfig().addDefault("messages.reloaded", (Object)"&aReloaded.");
        this.getConfig().addDefault("messages.mustwait", "&cYou must wait X seconds");
        this.getConfig().addDefault("messages.nolongercooldown", "&bNo longer cooldown, you can Double Jump");
        this.getConfig().addDefault("messages.noperm", "&cYou don't have the permission");
        this.getConfig().addDefault("messages.status-on", "&eYour Double Jump is ON!");
        this.getConfig().addDefault("messages.status-off", "&eYour Double Jump is OFF!");
        this.getConfig().addDefault("lobby.enabled", true);
        this.getConfig().addDefault("lobby.item-name", "&6&oDoubleJump");
        this.getConfig().addDefault("lobby.itemslot", 8);
        this.getConfig().addDefault("lobby.item-on", "BLAZE_ROD");
        this.getConfig().addDefault("lobby.item-off", "STICK");
        this.saveConfig();
    }
    
    
    @EventHandler
	public void onJoin(PlayerJoinEvent e) {
    	
		Player p = e.getPlayer();
		ItemStack js = new ItemStack(Material.matchMaterial(getConfig().getString("lobby.item-on")),1);
		ItemMeta jsm = js.getItemMeta();
		jsm.setDisplayName((this.getConfig().getString("lobby.item-name").replace("&", "\u00a7")));
		jsm.setLore(Arrays.asList("§7§oCurrent Status: ON"));
		js.setItemMeta(jsm);
		
		
		if (p.hasPermission("dj.use")) {
			this.canDJ.add(p.getName());	
		}
		
		if (this.getConfig().getBoolean("lobby.enabled") == true) {
			if (p.hasPermission("dj.use")) {
				p.getInventory().setItem((this.getConfig().getInt("lobby.itemslot")), js);
			}	
		}
    }
    
    @EventHandler
	public void onInventoryClick(PlayerInteractEvent e){
		Player p = e.getPlayer();
		ItemStack js = new ItemStack(Material.matchMaterial(getConfig().getString("lobby.item-on")),1);
		ItemMeta jsm = js.getItemMeta();
		ItemStack djs = new ItemStack(Material.matchMaterial(getConfig().getString("lobby.item-off")),1);
		ItemMeta djsm = djs.getItemMeta();
		

		jsm.setDisplayName((this.getConfig().getString("lobby.item-name").replace("&", "\u00a7")));
		djsm.setDisplayName((this.getConfig().getString("lobby.item-name").replace("&", "\u00a7")));

		jsm.setLore(Arrays.asList("§7§oCurrent Status: ON"));
		djsm.setLore(Arrays.asList("§7§oCurrent Status: OFF"));
		
		js.setItemMeta(jsm);
		djs.setItemMeta(djsm);
		
		if(e.getAction() == Action.RIGHT_CLICK_AIR) {
			if(p.hasPermission("dj.use")) {
			if(e.getItem().getType() == Material.matchMaterial(getConfig().getString("lobby.item-on"))) {
				p.chat("/dj toggle");
				p.getInventory().setItem((this.getConfig().getInt("lobby.itemslot")), djs);
			}
			if(e.getItem().getType() == Material.matchMaterial(getConfig().getString("lobby.item-off"))) {
				p.chat("/dj toggle");
				p.getInventory().setItem((this.getConfig().getInt("lobby.itemslot")), js);
			}
		}
	  }
   }
    
	@EventHandler
    public void onMove(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (!p.hasPermission("dj.use")) {
            return;
        }
        if (!this.canDJ(p)) {
            return;
        }
        if (!this.canDJ.contains(p.getName())) {
            return;
        }
        if (e.getPlayer().isOnGround()) {
            this.PlayersInAir.remove(p.getName());
        }
        if (this.PlayersInAir.contains(p.getName())) {
            p.getLocation().getWorld().playEffect(p.getLocation(), Effect.valueOf((String)this.getConfig().getString("settings.particle")), 2005);
        }
        if (e.getPlayer().getGameMode() != GameMode.CREATIVE && e.getPlayer().getLocation().getBlock().getRelative(BlockFace.DOWN).getType() != Material.AIR) {
            e.getPlayer().setAllowFlight(true);
        }
    }

    public boolean canDJ(Player p) {
        List<String> list = this.getConfig().getStringList("settings.worlds");
        for (String s : list) {
            if (Bukkit.getWorld((String)s) == null || !p.getWorld().getName().equalsIgnoreCase(s)) continue;
            return true;
        }
        return false;
    }

    @EventHandler
    public void onFly(PlayerToggleFlightEvent e) {
        Player p = e.getPlayer();
        if (!p.hasPermission("dj.use")) {
                return;
            }
        if (p.getGameMode() == GameMode.CREATIVE) {
            return;
        }
        if (this.cooldown.contains((Object)p)) {
        	if (!p.hasPermission("dj.bypasscooldown")) {
        		e.setCancelled(true);
        		p.playSound(p.getLocation(), Sound.valueOf((String)this.getConfig().getString("settings.waitsound")), 1.0f, -5.0f);
        		ActionBar.sendActionBar(p,(getConfig().getString("messages.mustwait").replace("&", "\u00a7")));
        		return;
        	}
        }
            this.PlayersInAir.add(p.getName());
            e.setCancelled(true);
            p.setAllowFlight(false);
            p.setFlying(false);
            p.setVelocity(p.getLocation().getDirection().multiply(1.0 * this.multiply).setY(1.0 * this.height));
            p.setNoDamageTicks(300);
            p.setFoodLevel(20);
            p.setHealth(20.0);
            p.setFallDistance(0.0f);
            p.getLocation().getWorld().playSound(p.getLocation(), Sound.valueOf((String)this.getConfig().getString("settings.sound")), 1.0f, -5.0f);
            this.cooldown.add(p);
 
        Bukkit.getServer().getScheduler().scheduleSyncDelayedTask((Plugin)this, new Runnable(){

        @Override
        public void run() {
            DoubleJump.this.cooldown.remove((Object)p);
            if(!p.hasPermission("dj.bypasscooldown")) {
            ActionBar.sendActionBar(p,(getConfig().getString("messages.nolongercooldown").replace("&", "\u00a7")));
            }
         }
    }, (this.getConfig().getInt("settings.cooldown")));
}
    
    
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        Player p = (Player)sender;
        if (cmd.getName().equalsIgnoreCase("dj")) {
            if (args.length == 0) {
                sender.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.specify-argument").replace("&", "\u00a7"));
                return false;
            }
            if (args[0].equalsIgnoreCase("reload") && p.hasPermission("dj.reload")) {
                p.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.reloaded").replace("&", "\u00a7"));
                this.reloadConfig();
            }
            if (args[0].equalsIgnoreCase("help") && p.hasPermission("dj.help")) {
                p.sendMessage("§DJ §7» §bShowing Help");
                p.sendMessage("§6DJ §7» §b/dj reload §7→ §bReload the Config");
                p.sendMessage("§6DJ §7» §b/dj help §7→ §bShow the help");
                return true;
            }
            if (args[0].equalsIgnoreCase("toggle") && p.hasPermission("dj.toggle")) {
                if (!this.canDJ.contains(p.getName())) {
                    this.canDJ.add(p.getName());
                    p.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.toggle-on").replace("&", "\u00a7"));
                } else {
                    this.canDJ.remove(p.getName());
                    p.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.toggle-off").replace("&", "\u00a7"));
                }
        		}
            }
            if (args[0].equalsIgnoreCase("status") && p.hasPermission("dj.status")) {
            	 if (!this.canDJ.contains(p.getName())) {
                     p.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.status-off").replace("&", "\u00a7"));
                 } else {
                     p.sendMessage(String.valueOf(this.getConfig().getString("messages.prefix").replace("&", "\u00a7")) + " " + this.getConfig().getString("messages.status-on").replace("&", "\u00a7"));
                 }
            }
		return false;
    }
      
}
